package com.example.joshua.studychat;

/**
 * Created by Joshua on 3/4/2016.
 */

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.Query;

public class ChatListAdapter extends FirebaseListAdapter<Chat> {

    private String mUsername;

    public ChatListAdapter(Query ref, Activity activity, int layout, String mUsername) {
        super(ref, Chat.class, layout, activity);
        this.mUsername = mUsername;
    }

    @Override
    protected void populateView(View view, Chat chat) {

        String author = chat.getAuthor();
        TextView authorText = (TextView) view.findViewById(R.id.author);
        authorText.setText(author + ": ");

        if(author != null && author.equals(mUsername)) {
            authorText.setTextColor(Color.CYAN);
        } else {
            authorText.setTextColor(Color.GRAY);
        }

        ((TextView) view.findViewById(R.id.message)).setText(chat.getMessage());
    }
}
