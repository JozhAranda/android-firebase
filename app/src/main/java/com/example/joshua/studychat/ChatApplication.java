package com.example.joshua.studychat;

import com.firebase.client.Firebase;

/**
 * Created by Joshua on 3/4/2016.
 */
public class ChatApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
