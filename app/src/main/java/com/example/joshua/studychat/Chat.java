package com.example.joshua.studychat;

/**
 * Created by Joshua on 3/4/2016.
 */
public class Chat {

    private String message;
    private String author;

    private Chat() {}

    Chat(String message, String author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() { return message; }
    public String getAuthor() { return author; }
}
